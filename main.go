package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strconv"
)

type ID int

type Payload struct {
	ID ID `json:"id"`
}

type Response struct {
	Meta struct {
		CurrentBest   int    `json:"current_best"`
		LastFormatted string `json:"last_formatted"`
	} `json:"meta"`
}

func extractID(rawurl string) (ID, error) {
	url, err := url.Parse(rawurl)
	if err != nil {
		return 0, err
	}
	re := regexp.MustCompile(`^.*-a(\d+).html$`)
	matches := re.FindStringSubmatch(url.Path)
	if len(matches) != 2 {
		return 0, fmt.Errorf("could not match id")
	}

	id, err := strconv.Atoi(matches[1])
	if err != nil {
		return 0, err
	}

	return ID(id), nil
}

func main() {
	flagURL := flag.String("url", "", "geizhals website url (like https://geizhals.de/amd-epyc-7742-100-100000053wof-a2117291.html)")
	flagLimit := flag.Int("limit", 1, "price limit. prices below or equal this will cause the program to exit with exit code two (2)")
	flag.Parse()

	parsedID, err := extractID(*flagURL)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	payload := Payload{
		ID: parsedID,
	}
	payloadBytes, err := json.Marshal(payload)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	body := bytes.NewReader(payloadBytes)

	req, err := http.NewRequest("POST", "https://geizhals.de/api/gh0/price_history", body)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	req.Header.Set("Content-Type", "application/json")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer resp.Body.Close()

	respBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	var data Response
	err = json.Unmarshal(respBytes, &data)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	limit := *flagLimit
	currPrice := data.Meta.CurrentBest

	if currPrice != 0 && currPrice <= limit {
		fmt.Println(currPrice)
		os.Exit(2)
	}
}
